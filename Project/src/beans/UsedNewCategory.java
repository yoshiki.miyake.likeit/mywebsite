package beans;

public class UsedNewCategory {
	
	private int usedNewCategoryId;
	private String usedNewCategoryName;
	public int getUsedNewCategoryId() {
		return usedNewCategoryId;
	}
	public void setUsedNewCategoryId(int usedNewCategoryId) {
		this.usedNewCategoryId = usedNewCategoryId;
	}
	public String getUsedNewCategoryName() {
		return usedNewCategoryName;
	}
	public void setUsedNewCategoryName(String usedNewCategoryName) {
		this.usedNewCategoryName = usedNewCategoryName;
	}
	

}
