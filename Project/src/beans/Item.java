package beans;

public class Item {

	/*
	 * `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_category_id` varchar(256) NOT NULL,
  `used_new_category_id` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `detail` text,
  `price` int(11) DEFAULT NULL,
  `file_name` varchar(256) DEFAULT NULL,
	 */

	private int itemId;
	private int itemCategoryId;
	private int usneCategoryId;
	private String itemName;
	private String itemDetail;
	private int price;
	private String PRICE;

	private String fileName;

	private String categoryName;
	private String usneCategoryName;

	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getItemCategoryId() {
		return itemCategoryId;
	}
	public void setItemCategoryId(int itemCategoryId) {
		this.itemCategoryId = itemCategoryId;
	}
	public int getUsneCategoryId() {
		return usneCategoryId;
	}
	public void setUsneCategoryId(int usneCategoryId) {
		this.usneCategoryId = usneCategoryId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemDetail() {
		return itemDetail;
	}
	public void setItemDetail(String itemDetail) {
		this.itemDetail = itemDetail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}

	public String getPRICE() {
		String PRICE = String.format("%,d", this.price);
		return PRICE;
	}

	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getUsneCategoryName() {
		return usneCategoryName;
	}
	public void setUsneCategoryName(String usneCategoryName) {
		this.usneCategoryName = usneCategoryName;
	}




}
