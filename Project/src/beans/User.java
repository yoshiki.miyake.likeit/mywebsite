package beans;

import java.util.Date;



public class User {

	private int userId;
	private String loginId;
	private String password;
	private String name;
	private Date birthDate;
	private String tel;
	private String mail;
	private String postalCode;
	private String prefectures;
	private String city;
	private String addapaName;
	private Date createDate;

	public User() {}

	public User(int userId, String loginId, String name) {
		this.userId = userId;
		this.loginId = loginId;
		this.name = name;
	}
	public User(int userId, String loginId, String name, Date birthDate, String postalCode, String prefectures) {
		this.userId = userId;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.postalCode = postalCode;
		this.prefectures = prefectures;
	}
	public User(int userId, String loginId, String name, Date birthDate, String tel, String mail, String postalCode, String prefectures, String city, String addapaName, Date createDate) {
		this.userId = userId;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.tel = tel;
		this.mail = mail;
		this.postalCode = postalCode;
		this.prefectures = prefectures;
		this.city = city;
		this.addapaName = addapaName;
		this.createDate = createDate;
	}


	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getPrefectures() {
		return prefectures;
	}
	public void setPrefectures(String prefectures) {
		this.prefectures = prefectures;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddapaName() {
		return addapaName;
	}
	public void setAddapaName(String addapaName) {
		this.addapaName = addapaName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


}
