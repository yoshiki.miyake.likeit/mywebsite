package maywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDAO;

/**
 * Servlet implementation class Signup
 */
@WebServlet("/Signup")
public class Signup extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Signup() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/signup.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		String loginId = request.getParameter("login_id");
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String tel = request.getParameter("tel");
		String mail = request.getParameter("mail");
		String postalCode = request.getParameter("address1");
		String prefectures = request.getParameter("address2");
		String city = request.getParameter("address3");
		String addapaName = request.getParameter("address4");

		UserDAO userDao = new UserDAO();
		if(! password1.equals(password2)) {
			request.setAttribute("errMsg", "パスワードが異なるため、新規登録に失敗しました。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/signup.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(loginId.equals("") || name.equals("") || birthDate.equals("")) {
			request.setAttribute("errMsg", "記載必須項目が空欄だったため、新規登録に失敗しました。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/signup.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(userDao.uniqueConstrint(loginId) == true) {
			request.setAttribute("errMsg", "先ほどのログインIDはすでに存在しているため、新規登録に失敗しました。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/signup.jsp");
			dispatcher.forward(request, response);
			return;
		}
		userDao.insertUser(loginId, password1, name, birthDate, tel, mail, postalCode, prefectures, city, addapaName);
		response.sendRedirect("Login");
	}

}
