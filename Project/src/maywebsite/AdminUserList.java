package maywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class AdminUserList
 */
@WebServlet("/AdminUserList")
public class AdminUserList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminUserList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			UserDAO userdao = new UserDAO();
			List<User> userList = userdao.allUser();
			request.setAttribute("userList", userList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/adminuserlist.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			String loginId = request.getParameter("loginId");
			String name = request.getParameter("name");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			String address1 = request.getParameter("address1");
			String address2 = request.getParameter("address2");
			UserDAO userdao = new UserDAO();
			List<User> userList = userdao.searchUser(loginId, name, startDate, endDate, address1, address2);
			request.setAttribute("userList", userList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/adminuserlist.jsp");
			dispatcher.forward(request, response);
		}
	}

}
