package maywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Item;
import beans.User;
import dao.ItemDAO;

/**
 * Servlet implementation class AdminItemDelete
 */
@WebServlet("/AdminItemDelete")
public class AdminItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminItemDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		//ログインセッションがなければログイン画面へ
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			String categoryId =(String) session.getAttribute("categoryId");
			if(categoryId != null) {
				session.removeAttribute("categoryId");
			}
			//アイテムIDの値を取得
			int itemId = Integer.parseInt(request.getParameter("id"));
			ItemDAO itemdao = new ItemDAO();
			//アイテムIDを元にアイテム情報をセッションに保存
			Item item = itemdao.detailItem(itemId);
			session.setAttribute("item", item);
			//商品削除画面へフォワード
			RequestDispatcher dp = request.getRequestDispatcher("WEB-INF/jsp/adminitemdelete.jsp");
			dp.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		//ログインセッションがなければログイン画面へ
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			//Getメソッドで保存したセッションを取得
			Item item = (Item) session.getAttribute("item");
			ItemDAO itemdao = new ItemDAO();
			//セッションのアイテムIDでDBのアイテムを削除
			itemdao.deleteItem(item.getItemId());
			//アイテム情報セッションを削除
			session.removeAttribute("item");
			//アイテムリストにリダイレクト
			response.sendRedirect("AdminItemList");
		}
	}

}
