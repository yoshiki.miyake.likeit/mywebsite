package maywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userInfo");
		//ログインセッションが残っていればアイテムリスト画面へ遷移
		if(user != null) {
			if(user.getLoginId().equals("admin")) {
				response.sendRedirect("AdminItemList");
			} else {
				response.sendRedirect("ItemList");
			}
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		//ログイン画面で入力した値を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		UserDAO userDao = new UserDAO();
		//ログイン確認
		User user = userDao.loginInfo(loginId, password);
		//ログインID等がDBになければ、ログイン画面に遷移
		if(user == null) {
			request.setAttribute("errMsg", "ログインに失敗しました。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}
		HttpSession session = request.getSession();
		//ログイン情報をセッションに保存
		session.setAttribute("userInfo", user);
		//管理者とそれ以外の遷移を分ける
		if(user.getLoginId().equals("admin")) {
			response.sendRedirect("AdminItemList");
		} else {
			response.sendRedirect("ItemList");
		}
	}

}
