package maywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/AdminUserUpdate")
public class AdminUserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminUserUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			String userId = request.getParameter("id");
			UserDAO userdao = new UserDAO();
			User user = userdao.userDetail(userId);
			session.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/adminuserupdate.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			String password1 = request.getParameter("password1");
			String password2 = request.getParameter("password2");
			String name = request.getParameter("name");
			String birthDate = request.getParameter("birthDate");
			String tel = request.getParameter("tel");
			String mail = request.getParameter("mail");
			String postalCode = request.getParameter("address1");
			String prefectures = request.getParameter("address2");
			String city = request.getParameter("address3");
			String addapaName = request.getParameter("address4");
			String id = request.getParameter("id");

			User user = (User)session.getAttribute("user");

			UserDAO userDao = new UserDAO();
			if(! password1.equals(password2)) {
				request.setAttribute("errMsg", "パスワードが異なるため、更新に失敗しました。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminuserupdate.jsp");
				dispatcher.forward(request, response);
				return;
			}
			if(name.equals("") || birthDate.equals("")) {
				request.setAttribute("errMsg", "記載必須項目が空欄だったため、更新に失敗しました。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminuserupdate.jsp");
				dispatcher.forward(request, response);
				return;
			}

			if(password1.equals("") && password2.equals("")) {
				userDao.updateupUser(name, birthDate, tel, mail, postalCode, prefectures, city, addapaName, user.getUserId());
				session.removeAttribute("user");
				response.sendRedirect("AdminUserList");
			} else {
				userDao.updateUser(password1, name, birthDate, tel, mail, postalCode, prefectures, city, addapaName, user.getUserId());
				session.removeAttribute("user");
				response.sendRedirect("AdminUserList");
			}
		}
	}
}
