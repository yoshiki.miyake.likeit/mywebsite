package maywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Buy;
import beans.User;
import dao.BuyDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserDetail
 */
@WebServlet("/AdminUserDetail")
public class AdminUserDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminUserDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			String userId = request.getParameter("id");
			UserDAO userdao = new UserDAO();
			User user = userdao.userDetail(userId);
			request.setAttribute("user", user);

			BuyDAO buydao = new BuyDAO();
			List<Buy> buyList = buydao.getBuyList(Integer.parseInt(userId));
			request.setAttribute("buyList", buyList);


			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/adminuserdetail.jsp");
			dispatcher.forward(request, response);
		}
	}

}
