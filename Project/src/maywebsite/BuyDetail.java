package maywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Buy;
import beans.Item;
import beans.User;
import dao.BuyDAO;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class BuyDetail
 */
@WebServlet("/BuyDetail")
public class BuyDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		//ログインセッションがなければログイン画面へ
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			int buyId = Integer.parseInt(request.getParameter("id"));

			BuyDetailDAO bddao = new BuyDetailDAO();
			BuyDAO bdao = new BuyDAO();

			List<Item> buyitemList = bddao.buyItemList(buyId);
			request.setAttribute("buyitemList", buyitemList);

			Buy buy = bdao.getBuyId(buyId);
			request.setAttribute("buy", buy);

			RequestDispatcher dp = request.getRequestDispatcher("WEB-INF/jsp/buydetail.jsp");
			dp.forward(request, response);
		}
	}

}
