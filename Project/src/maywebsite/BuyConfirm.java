package maywebsite;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Buy;
import beans.BuyDetail;
import beans.DeliveryMethod;
import beans.Item;
import beans.User;
import dao.BuyDAO;
import dao.BuyDetailDAO;
import dao.DeliveryMethodDAO;

/**
 * Servlet implementation class Buy
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}
	public static int getTotalItemPrice(List<Item> items) {
		int total = 0;
		for (Item item : items) {
			total += item.getPrice();
		}
		return total;
	}

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");
		//ログインセッションがなければログイン画面へ
		if(user == null) {
			response.sendRedirect("Login");
		} else {
			List<Item> cart = (ArrayList<Item>) session.getAttribute("cart");
			String cartActionMessage = "";
			if(cart.size() == 0) {
				cartActionMessage = "購入品がありません";
				request.setAttribute("cartActionMessage", cartActionMessage);
				RequestDispatcher dp = request.getRequestDispatcher("WEB-INF/jsp/cart.jsp");
				dp.forward(request, response);
			}
			DeliveryMethodDAO dmdao = new DeliveryMethodDAO();
			DeliveryMethod DMB = dmdao.deliverDetail(1);

			int totalPrice = getTotalItemPrice(cart) + DMB.getDeliveryPrice();

			Buy bdb = new Buy();
			bdb.setUserId(user.getUserId());
			bdb.setTotalPrice(totalPrice);
			bdb.setDeliveryId(DMB.getDeliveryId());

			request.setAttribute("DMB", DMB);
			session.setAttribute("bdb", bdb);
			RequestDispatcher dp = request.getRequestDispatcher("WEB-INF/jsp/buy.jsp");
			dp.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		//ログインセッションがなければログイン画面へ
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			List<Item> cart = (ArrayList<Item>) cutSessionAttribute(session, "cart");
			Buy bdb = (Buy) cutSessionAttribute(session, "bdb");

			BuyDAO buydao = new BuyDAO();
			int buyId = buydao.insertBuy(bdb.getUserId(), bdb.getTotalPrice(), bdb.getDeliveryId());
			BuyDetailDAO BDD = new BuyDetailDAO();
			for(Item cartItem : cart) {
				BuyDetail buydetail = new BuyDetail();
				buydetail.setBuyId(buyId);
				buydetail.setItemId(cartItem.getItemId());
				BDD.insertBuyDetail(buydetail);
			}
			Buy buyData = buydao.getBuyId(buyId);
			request.setAttribute("buyData", buyData);

			List<Item> buyItemList = BDD.buyItemList(buyId);
			request.setAttribute("buyItemList", buyItemList);

			RequestDispatcher dp = request.getRequestDispatcher("WEB-INF/jsp/buyresult.jsp");
			dp.forward(request, response);
		}
	}

}
