package maywebsite;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Category;
import beans.Item;
import beans.UsedNewCategory;
import beans.User;
import dao.CategoryDAO;
import dao.ItemDAO;
import dao.UsedNewCategoryDAO;

/**
 * Servlet implementation class AdminItemList
 */
@WebServlet("/AdminItemList")
public class AdminItemList extends HttpServlet {
	private static final long serialVersionUID = 1L;


    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminItemList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		//ログインセッションがなければログイン画面へ遷移
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			String categoryId =(String) session.getAttribute("categoryId");
			if(categoryId != null) {
				session.removeAttribute("categoryId");
			}

			ItemDAO itemdao = new ItemDAO();

			//ランダムで３種類表示
			List<Item> itemList = itemdao.itemAll(3);
			request.setAttribute("itemList", itemList);

			//カテゴリー別でランダムで３種類表示
			Random random = new Random();
			int rd1 = random.nextInt(6) + 1;
			List<Item> categoryList = itemdao.categoryItem(rd1, 3);
			request.setAttribute("categoryList", categoryList);
			CategoryDAO CDAO = new CategoryDAO();
			Category cate = CDAO.cateAll(rd1);
			request.setAttribute("cate", cate);

			int rd2 = random.nextInt(2) + 1;
			List<Item> usnecategory = itemdao.usnecategoryItem(rd2, 3);
			request.setAttribute("usnecategory", usnecategory);
			UsedNewCategoryDAO usnedao = new UsedNewCategoryDAO();
			UsedNewCategory usne = usnedao.uncAll(rd2);
			request.setAttribute("usne", usne);

			String searchWord = (String)session.getAttribute("searchWord");
			//検索ワードのセッションが残っていれば消す
			if(searchWord != null) {
				session.removeAttribute("searchWord");
			}

			//アイテムリストへ遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminitemlist.jsp");
			dispatcher.forward(request, response);
		}
	}

}
