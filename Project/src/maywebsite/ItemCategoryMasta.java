package maywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Category;
import beans.Item;
import beans.User;
import dao.CategoryDAO;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemCategoryMasta
 */
@WebServlet("/ItemCategoryMasta")
public class ItemCategoryMasta extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final static int PAGE_MAX_ITEM_COUNT = 6;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemCategoryMasta() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			String searchWord = request.getParameter("search_word");
			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
			// 新たに検索されたキーワードをセッションに格納する
			session.setAttribute("searchWord", searchWord);

			// 商品リストを取得 ページ表示分のみ
			ItemDAO itemdao = new ItemDAO();

			String categoryId =(String) session.getAttribute("categoryId");

			if(categoryId == null) {
				String cateId = request.getParameter("id");
				session.setAttribute("categoryId", cateId);

				CategoryDAO cateDAO = new CategoryDAO();
				Category category = cateDAO.cateAll(Integer.parseInt(cateId));
				request.setAttribute("category",category);

				List<Item> searchItemList = itemdao.searchCategoryItem(searchWord, cateId, pageNum, PAGE_MAX_ITEM_COUNT);
				request.setAttribute("itemList", searchItemList);

				double itemCount = ItemDAO.getCategoryItemCount(cateId,searchWord);
				int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

				//総アイテム数
				request.setAttribute("itemCount", (int) itemCount);
				// 総ページ数
				request.setAttribute("pageMax", pageMax);
				// 表示ページ
				request.setAttribute("pageNum", pageNum);

			} else {

				request.setAttribute("categoryId", categoryId);

				CategoryDAO cateDAO = new CategoryDAO();
				Category category = cateDAO.cateAll(Integer.parseInt(categoryId));
				request.setAttribute("category",category);

				List<Item> searchItemList = itemdao.searchCategoryItem(searchWord, categoryId, pageNum, PAGE_MAX_ITEM_COUNT);
				request.setAttribute("itemList", searchItemList);

				// 検索ワードに対しての総ページ数を取得
				double itemCount = ItemDAO.getCategoryItemCount(categoryId,searchWord);
				int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

				//総アイテム数
				request.setAttribute("itemCount", (int) itemCount);
				// 総ページ数
				request.setAttribute("pageMax", pageMax);
				// 表示ページ
				request.setAttribute("pageNum", pageNum);
			}

			if(u.getLoginId().equals("admin")) {
				RequestDispatcher dp = request.getRequestDispatcher("WEB-INF/jsp/adminitemmastalist.jsp");
				dp.forward(request, response);
			} else {
				RequestDispatcher dp = request.getRequestDispatcher("WEB-INF/jsp/itemmastalist.jsp");
				dp.forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
