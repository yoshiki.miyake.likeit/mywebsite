package maywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.User;
import dao.ItemDAO;

/**
 * Servlet implementation class AdminItemInsert
 */
@WebServlet("/AdminItemInsert")
@MultipartConfig(location="/Users/miyakeyoshitaka/Documents/MyWebSite/Project/WebContent/img",maxFileSize=1048576)
public class AdminItemInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;

	 private String getFileName(Part part){
         String name = null;
         for(String dispotion : part.getHeader("Content-Disposition").split(";")){
             if(dispotion.trim().startsWith("filename")){
                 name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"","").trim();
                 name = name.substring(name.lastIndexOf("\\") + 1 );
                 break;
             }
         }
         return name;
   }

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminItemInsert() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		//ログインセッションがなければログイン画面へ
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			String categoryId =(String) session.getAttribute("categoryId");
			if(categoryId != null) {
				session.removeAttribute("categoryId");
			}
			RequestDispatcher dp = request.getRequestDispatcher("WEB-INF/jsp/adminiteminsert.jsp");
			dp.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("userInfo");
		//ログインセッションがなければログイン画面へ
		if(u == null) {
			response.sendRedirect("Login");
		} else {
			//アイテム登録画面で入力した値を取得
			String cateId = request.getParameter("itemCategoryId");
			String unId = request.getParameter("usneCategoryId");
			String name = request.getParameter("name");
			String detail = request.getParameter("detail");
			String price = request.getParameter("price");
			//String fileName = request.getParameter("ck01");

			Part part = request.getPart("ck01");
	        //part(主にjsp)から送られてきたファイル名を取得
	        String name1 = this.getFileName(part);
			try {
		        part.write(name1);
			} catch(Exception e) {
				//e.printStackTrace();
				name1 = "";
			}
			
			ItemDAO itemdao = new ItemDAO();
			//必須項目が空欄の場合はエラ〜メッセージを表示し再び入力画面へ
			if(cateId.equals("") || unId.equals("") || name.equals("")|| detail.equals("") || price.equals("") || name1.equals("")) {
				request.setAttribute("errMsg", "項目が空欄だったため、更新に失敗しました。");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/adminiteminsert.jsp");
				dispatcher.forward(request, response);
				return;
			}
			//取得した値を元にDBに登録
			itemdao.insertItem(Integer.parseInt(cateId), Integer.parseInt(unId), name, detail, Integer.parseInt(price), name1);
			//管理者用アイテムリストにリダイレクト
			response.sendRedirect("AdminItemList");
		}
	}
}
