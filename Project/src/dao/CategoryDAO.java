package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.Category;
import db.DBManager;

public class CategoryDAO {

	//表示
	public Category cateAll(int ctId) {

		Connection con = null;
		try {
			String sql = "SELECT * FROM category INNER JOIN m_item ON"
					+ " category.id = m_item.item_category_id WHERE category.id = ?";
			con = DBManager.getConnection();
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, ctId);
			ResultSet rs = st.executeQuery();
			Category cate = new Category();
			if(rs.next()) {
				 cate.setCategoryId(rs.getInt("id"));
				 cate.setCategoryName(rs.getString("category_name"));
			}
			return cate;
		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
