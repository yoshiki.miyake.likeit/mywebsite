package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.DeliveryMethod;
import db.DBManager;

public class DeliveryMethodDAO {

	//配送IDで検索
	public DeliveryMethod deliverDetail(int delId) {

		Connection con = null;
		PreparedStatement st = null;
		try {
			String sql = "SELECT * FROM m_delivery_method WHERE id = ?";
			con = DBManager.getConnection();
			st = con.prepareStatement(sql);
			st.setInt(1, delId);
			ResultSet rs = st.executeQuery();
			DeliveryMethod dm = new DeliveryMethod();
			if(rs.next()) {
				dm.setDeliveryId(rs.getInt("id"));
				dm.setDeliveryName(rs.getString("name"));
				dm.setDeliveryPrice(rs.getInt("price"));
			}
			return dm;
		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			} catch(SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

	//Buyの配送IDで検索
	public DeliveryMethod buyDeliverDetail(int buyDelId) {

		Connection con = null;
		try {
			String sql = "SELECT * FROM m_delivery_method INNER JOIN t_buy ON m_delivery_method.id = t_buy.delivery_method_id"
					+ " WHERE t_buy.delivery_method_id = ?";
			con = DBManager.getConnection();
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, buyDelId);
			ResultSet rs = st.executeQuery();
			DeliveryMethod dm = new DeliveryMethod();
			if(rs.next()) {
				dm.setDeliveryName(rs.getString("name"));
				dm.setDeliveryPrice(rs.getInt("m_delivery_method.price"));
			}
			return dm;
		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			} catch(SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

}
