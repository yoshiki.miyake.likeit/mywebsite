package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.UsedNewCategory;
import db.DBManager;

public class UsedNewCategoryDAO {

	//表示
	public UsedNewCategory uncAll(int unId) {

		Connection con = null;

		try {
			String sql = "SELECT * FROM used_new_category INNER JOIN m_item ON"
					+ " used_new_category.id = m_item.used_new_category_id WHERE used_new_category.id = ?";
			con = DBManager.getConnection();
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, unId);
			ResultSet rs = st.executeQuery();
			UsedNewCategory unc = new UsedNewCategory();
			if(rs.next()) {
				unc.setUsedNewCategoryId(rs.getInt("id"));
				unc.setUsedNewCategoryName(rs.getString("used_new_category_name"));
			}
			return unc;
		} catch(SQLException e ) {
			e.printStackTrace();
			return null;
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
