package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import beans.User;
import db.DBManager;

public class UserDAO {

	//ログイン用メソッド
	public User loginInfo(String loginId, String password) {

		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM t_user WHERE login_id = ? and password = ?";

			PreparedStatement stmt = conn.prepareStatement(sql);
			String al = algo(password);

			stmt.setString(1, loginId);
			stmt.setString(2, al);
			ResultSet rs = stmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int userId = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(userId, loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー新規登録のUNIQUE確認
	public boolean uniqueConstrint(String loginId) {

		Connection conn = null;

		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM t_user WHERE login_id = ?";
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, loginId);
			ResultSet rs = stmt.executeQuery();
			if (!rs.next()) {
				return false;
			}
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return false;
				}
			}
		}
	}

	//ユーザー新規登録用のメソッド
	public void insertUser(String loginId, String password, String name, String birthDate, String tel, String mail,
			String postalCode, String prefectures, String city, String addapaName) {

		Connection con = null;
		PreparedStatement stmt = null;

		try {
			String sql = "INSERT INTO t_user (login_id, password, name, birth_data, tel, mail, postal_code, prefectures, city, address_apartment_name, create_date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, now())";

			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);
			String al = algo(password);

			stmt.setString(1, loginId);
			stmt.setString(2, al);
			stmt.setString(3, name);
			stmt.setString(4, birthDate);
			stmt.setString(5, tel);
			stmt.setString(6, mail);
			stmt.setString(7, postalCode);
			stmt.setString(8, prefectures);
			stmt.setString(9, city);
			stmt.setString(10, addapaName);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//全検索用
	public List<User> allUser() {

		Connection conn = null;
		List<User> userList = new ArrayList<>();
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM t_user";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_data");
				String postalCode = rs.getString("postal_code");
				String prefectures = rs.getString("prefectures");
				User user = new User(id, loginId, name, birthDate, postalCode, prefectures);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//ユーザーID検索（管理者用）
	public User userDetail(String id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM t_user WHERE id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, id);
			ResultSet rs = stmt.executeQuery();
			while (!rs.next()) {
				return null;
			}
			int userId = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_data");
			String tel = rs.getString("tel");
			String mail = rs.getString("mail");
			String postalCode = rs.getString("postal_code");
			String prefectures = rs.getString("prefectures");
			String city = rs.getString("city");
			String addapaName = rs.getString("address_apartment_name");
			Date createDate = rs.getDate("create_date");

			User user = new User(userId, loginId, name, birthDate, tel, mail, postalCode, prefectures, city, addapaName,
					createDate);
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザーID検索
	public User userDetail(int id) {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM t_user WHERE id = ?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while (!rs.next()) {
				return null;
			}
			int userId = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_data");
			String tel = rs.getString("tel");
			String mail = rs.getString("mail");
			String postalCode = rs.getString("postal_code");
			String prefectures = rs.getString("prefectures");
			String city = rs.getString("city");
			String addapaName = rs.getString("address_apartment_name");
			Date createDate = rs.getDate("create_date");

			User user = new User(userId, loginId, name, birthDate, tel, mail, postalCode, prefectures, city, addapaName,
					createDate);
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ユーザー更新
	public void updateUser(String password, String name, String birthDate, String tel, String mail, String postalCode,
			String prefectures, String city, String addapaName, int id) {

		Connection con = null;
		PreparedStatement stmt = null;

		try {
			String sql = "UPDATE t_user SET password = ?, name = ?, birth_data = ?, tel = ?, mail = ?, postal_code = ?, prefectures = ?, city = ?, address_apartment_name = ? WHERE id = ?";

			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);
			String al = algo(password);

			stmt.setString(1, al);
			stmt.setString(2, name);
			stmt.setString(3, birthDate);
			stmt.setString(4, tel);
			stmt.setString(5, mail);
			stmt.setString(6, postalCode);
			stmt.setString(7, prefectures);
			stmt.setString(8, city);
			stmt.setString(9, addapaName);
			stmt.setInt(10, id);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//パスワード以外の更新
	public void updateupUser(String name, String birthDate, String tel, String mail, String postalCode,
			String prefectures, String city, String addapaName, int id) {

		Connection con = null;
		PreparedStatement stmt = null;

		try {
			String sql = "UPDATE t_user SET name = ?, birth_data = ?, tel = ?, mail = ?, postal_code = ?, prefectures = ?, city = ?, address_apartment_name = ? WHERE id = ?";

			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, birthDate);
			stmt.setString(3, tel);
			stmt.setString(4, mail);
			stmt.setString(5, postalCode);
			stmt.setString(6, prefectures);
			stmt.setString(7, city);
			stmt.setString(8, addapaName);
			stmt.setInt(9, id);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//ユーザー削除画面
	public void deleteUser(int id) {
		Connection con = null;
		PreparedStatement stmt = null;
		try {
			String sql = "DELETE FROM t_user WHERE id = ?";
			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, id);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//ユーザー検索用
	public List<User> searchUser(String loginId, String name, String startDate, String endDate, String postalCode,
			String prefectures) {
		Connection con = null;
		List<User> userList = new ArrayList<>();
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM t_user WHERE id NOT IN (1)";
			if (!loginId.equals("")) {
				sql += " AND login_id = '" + loginId + "'";
			}
			if (!name.equals("")) {
				sql += " AND name LIKE '%" + name + "%'";
			}
			if (!(startDate.equals("") && endDate.equals(""))) {
				sql += " AND birth_date BETWEEN'" + startDate + "' AND  '" + endDate + "'";
			}
			if (!postalCode.equals("")) {
				sql += "AND postal_code = '" + postalCode + "'";
			}
			if (!prefectures.equals("")) {
				sql += "AND prefectures = '" + prefectures + "'";
			}
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginIdData = rs.getString("login_id");
				String nameData = rs.getString("name");
				Date birthDate = rs.getDate("birth_data");
				String postalData = rs.getString("postal_code");
				String prefectuData = rs.getString("prefectures");
				User user = new User(id, loginIdData, nameData, birthDate, postalData, prefectuData);
				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//パスワードのMD5暗号化
	public String algo(String password) {

		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		return result;
	}

}
