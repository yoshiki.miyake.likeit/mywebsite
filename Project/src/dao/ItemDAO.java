package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Item;
import db.DBManager;

public class ItemDAO {

	//商品登録
	public void insertItem(int itemCategoryId, int usneCategoryId, String itemName, String detail, int price,
			String fileName) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			String sql = "INSERT INTO m_item (item_category_id, used_new_category_id, name, detail, price, file_name) VALUES (?, ?, ?, ?, ?, ?)";
			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);

			stmt.setInt(1, itemCategoryId);
			stmt.setInt(2, usneCategoryId);
			stmt.setString(3, itemName);
			stmt.setString(4, detail);
			stmt.setInt(5, price);
			stmt.setString(6, fileName);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//商品情報更新
	public void updateItem(String itemName, String Detail, int price, String fileName, int itemId) {

		Connection con = null;
		PreparedStatement stmt = null;

		try {
			String sql = "UPDATE m_item SET name = ?, detail = ?, price = ?, file_name = ? WHERE id = ?";
			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);

			stmt.setString(1, itemName);
			stmt.setString(2, Detail);
			stmt.setInt(3, price);
			stmt.setString(4, fileName);
			stmt.setInt(5, itemId);

			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//商品削除
	public void deleteItem(int itemId) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			String sql = "DELETE FROM m_item  WHERE id = ?";
			con = DBManager.getConnection();
			stmt = con.prepareStatement(sql);

			stmt.setInt(1, itemId);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	//商品詳細
	public Item detailItem(int itemId) {

		Connection con = null;
		try {
			String sql = "SELECT * FROM m_item INNER JOIN category ON m_item.item_category_id = category.id"
					+ " INNER JOIN used_new_category ON m_item.used_new_category_id = used_new_category.id"
					+ " WHERE m_item.id = ?";
			con = DBManager.getConnection();
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, itemId);
			ResultSet rs = st.executeQuery();
			while(!rs.next()) {
				return null;
			}
			Item item = new Item();
			item.setItemId(rs.getInt("id"));
			item.setItemCategoryId(rs.getInt("item_category_id"));
			item.setUsneCategoryId(rs.getInt("used_new_category_id"));
			item.setItemName(rs.getString("name"));
			item.setItemDetail(rs.getString("detail"));
			item.setPrice(rs.getInt("price"));
			item.setFileName(rs.getString("file_name"));
			item.setCategoryName(rs.getString("category_name"));
			item.setUsneCategoryName(rs.getString("used_new_category_name"));
			return item;
		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	//アイテムリスト
	public List<Item> itemAll(int randomItem) {

		Connection con = null;
		List<Item> itemList = new ArrayList<>();
		try {
			String sql = "SELECT * FROM m_item INNER JOIN category ON m_item.item_category_id = category.id"
					+ " INNER JOIN used_new_category ON m_item.used_new_category_id = used_new_category.id"
					+ " ORDER BY RAND() LIMIT ?";
			con = DBManager.getConnection();
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, randomItem);
			ResultSet rs = st.executeQuery();

			while(rs.next()) {
				Item item = new Item();
				item.setItemId(rs.getInt("id"));
				item.setItemCategoryId(rs.getInt("item_category_id"));
				item.setUsneCategoryId(rs.getInt("used_new_category_id"));
				item.setItemName(rs.getString("name"));
				item.setItemDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setCategoryName(rs.getString("category_name"));
				item.setUsneCategoryName(rs.getString("used_new_category_name"));
				itemList.add(item);
			}

		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//カテゴリー別表示
	public List<Item> categoryItem(int categoryId, int randomItem) {

		Connection con = null;
		List<Item> itemList = new ArrayList<>();
		try {
			String sql = "SELECT * FROM m_item INNER JOIN category ON m_item.item_category_id = category.id"
					+ " INNER JOIN used_new_category ON m_item.used_new_category_id = used_new_category.id"
					+ " WHERE item_category_id = ?"
					+ " ORDER BY RAND() LIMIT ?";
			con = DBManager.getConnection();
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, categoryId);
			st.setInt(2, randomItem);
			ResultSet rs = st.executeQuery();

			while(rs.next()) {
				Item item = new Item();
				item.setItemId(rs.getInt("id"));
				item.setItemCategoryId(rs.getInt("item_category_id"));
				item.setUsneCategoryId(rs.getInt("used_new_category_id"));
				item.setItemName(rs.getString("name"));
				item.setItemDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setCategoryName(rs.getString("category_name"));
				item.setUsneCategoryName(rs.getString("used_new_category_name"));
				itemList.add(item);
			}

		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//商品状態カテゴリー別表示
		public List<Item> usnecategoryItem(int usnecategoryId, int randomItem) {

			Connection con = null;
			List<Item> itemList = new ArrayList<>();
			try {
				String sql = "SELECT * FROM m_item INNER JOIN category ON m_item.item_category_id = category.id"
						+ " INNER JOIN used_new_category ON m_item.used_new_category_id = used_new_category.id"
						+ " WHERE used_new_category_id = ?"
						+ " ORDER BY RAND() LIMIT ?";
				con = DBManager.getConnection();
				PreparedStatement st = con.prepareStatement(sql);
				st.setInt(1, usnecategoryId);
				st.setInt(2, randomItem);
				ResultSet rs = st.executeQuery();

				while(rs.next()) {
					Item item = new Item();
					item.setItemId(rs.getInt("id"));
					item.setItemCategoryId(rs.getInt("item_category_id"));
					item.setUsneCategoryId(rs.getInt("used_new_category_id"));
					item.setItemName(rs.getString("name"));
					item.setItemDetail(rs.getString("detail"));
					item.setPrice(rs.getInt("price"));
					item.setFileName(rs.getString("file_name"));
					item.setCategoryName(rs.getString("category_name"));
					item.setUsneCategoryName(rs.getString("used_new_category_name"));
					itemList.add(item);
				}

			} catch(SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				if(con != null) {
					try {
						con.close();
					} catch(SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return itemList;
		}


	//検索
	public List<Item> searchItem(String searchWord, int pageNum, int pageMaxItemCount) {

		Connection con = null;
		List<Item> itemList = new ArrayList<>();
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();
			if (searchWord.length() == 0) {
				String sql = "SELECT * FROM m_item INNER JOIN category ON m_item.item_category_id = category.id"
						+ " INNER JOIN used_new_category ON m_item.used_new_category_id = used_new_category.id"
						+ " ORDER BY m_item.id ASC LIMIT ?,?";
				// 全検索
				st = con.prepareStatement(sql);
				st.setInt(1, startiItemNum);
				st.setInt(2, pageMaxItemCount);
			} else {
				String sql = "SELECT * FROM m_item INNER JOIN category ON m_item.item_category_id = category.id"
						+ " INNER JOIN used_new_category ON m_item.used_new_category_id = used_new_category.id"
						+ " WHERE name LIKE ?"
						+ " ORDER BY m_item.id ASC LIMIT ?,?";
				// 商品名検索
				st = con.prepareStatement(sql);
				st.setString(1, "%" + searchWord + "%");
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			}
			ResultSet rs = st.executeQuery();

			while(rs.next()) {
				Item item = new Item();
				item.setItemId(rs.getInt("id"));
				item.setItemCategoryId(rs.getInt("item_category_id"));
				item.setUsneCategoryId(rs.getInt("used_new_category_id"));
				item.setItemName(rs.getString("name"));
				item.setItemDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setCategoryName(rs.getString("category_name"));
				item.setUsneCategoryName(rs.getString("used_new_category_name"));
				itemList.add(item);
			}

		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//カテゴリー検索
	public List<Item> searchCategoryItem(String searchWord, String categoryId, int pageNum, int pageMaxItemCount) {

		Connection con = null;
		List<Item> itemList = new ArrayList<>();
		PreparedStatement st = null;
		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();
			if (searchWord == null) {
				String sql = "SELECT * FROM m_item INNER JOIN category ON m_item.item_category_id = category.id"
						+ " INNER JOIN used_new_category ON m_item.used_new_category_id = used_new_category.id"
						+ " WHERE item_category_id = ?"
						+ " ORDER BY m_item.id ASC LIMIT ?,?";
				// 全検索
				st = con.prepareStatement(sql);
				st.setString(1, categoryId);
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			} else if (searchWord.length() == 0) {
				String sql = "SELECT * FROM m_item INNER JOIN category ON m_item.item_category_id = category.id"
						+ " INNER JOIN used_new_category ON m_item.used_new_category_id = used_new_category.id"
						+ " WHERE item_category_id = ?"
						+ " ORDER BY m_item.id ASC LIMIT ?,?";
				// 全検索
				st = con.prepareStatement(sql);
				st.setString(1, categoryId);
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			} else {
				String sql = "SELECT * FROM m_item INNER JOIN category ON m_item.item_category_id = category.id"
						+ " INNER JOIN used_new_category ON m_item.used_new_category_id = used_new_category.id"
						+ " WHERE name LIKE ? and item_category_id = ?"
						+ " ORDER BY m_item.id ASC LIMIT ?,?";
				// 商品名検索
				st = con.prepareStatement(sql);
				st.setString(1, "%" + searchWord + "%");
				st.setString(2, categoryId);
				st.setInt(3, startiItemNum);
				st.setInt(4, pageMaxItemCount);
			}
			ResultSet rs = st.executeQuery();

			while(rs.next()) {
				Item item = new Item();
				item.setItemId(rs.getInt("id"));
				item.setItemCategoryId(rs.getInt("item_category_id"));
				item.setUsneCategoryId(rs.getInt("used_new_category_id"));
				item.setItemName(rs.getString("name"));
				item.setItemDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setCategoryName(rs.getString("category_name"));
				item.setUsneCategoryName(rs.getString("used_new_category_name"));
				itemList.add(item);
			}

		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch(SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	public static double getItemCount(String searchWord){
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from m_item where name like ?");
			st.setString(1, "%" + searchWord + "%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (SQLException e) {
			e.printStackTrace();
			return java.lang.Double.NaN;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch(SQLException e) {
					e.printStackTrace();
					return java.lang.Double.NaN;
				}
			}
		}
	}

	public static double getCategoryItemCount(String categoryId,String searchWord){
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			if(searchWord == null) {
				st = con.prepareStatement("select count(*) as cnt from m_item where item_category_id = ?");
				st.setString(1, categoryId);
			} else {
				st = con.prepareStatement("select count(*) as cnt from m_item where item_category_id = ? and name like ?");
				st.setString(1, categoryId);
				st.setString(2, "%" + searchWord + "%");
			}
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (SQLException e) {
			e.printStackTrace();
			return java.lang.Double.NaN;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch(SQLException e) {
					e.printStackTrace();
					return java.lang.Double.NaN;
				}
			}
		}
	}
}
