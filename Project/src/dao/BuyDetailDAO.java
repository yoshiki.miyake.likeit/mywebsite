package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.BuyDetail;
import beans.Item;
import db.DBManager;

public class BuyDetailDAO {

	//購入品詳細登録
	public void insertBuyDetail(BuyDetail buydetail) {

		Connection con = null;
		PreparedStatement st = null;

		try {
			String sql = "INSERT INTO t_buy_detail (buy_id, item_id) VALUES (?, ?)";
			con = DBManager.getConnection();
			st = con.prepareStatement(sql);
			st.setInt(1, buydetail.getBuyId());
			st.setInt(2, buydetail.getItemId());
			st.executeUpdate();

		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}
	//buyIdを元に購入品リストを表示
	public List<Item> buyItemList(int buyId) {

		Connection con = null;
		PreparedStatement st = null;
		try {
			String sql = "SELECT m_item.id,"
					+ " m_item.name,"
					+ " m_item.price"
					+ " FROM t_buy_detail"
					+ " JOIN m_item"
					+ " ON t_buy_detail.item_id = m_item.id"
					+ " WHERE t_buy_detail.buy_id = ?";
			con = DBManager.getConnection();
			st = con.prepareStatement(sql);
			st.setInt(1, buyId);
			ResultSet rs = st.executeQuery();
			List<Item> buyItemList = new ArrayList<>();
			while(rs.next()) {
				Item item = new Item();
				item.setItemId(rs.getInt("id"));
				item.setItemName(rs.getString("name"));
				item.setPrice(rs.getInt("price"));
				buyItemList.add(item);
			}
			return buyItemList;
		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			} catch(SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

}
