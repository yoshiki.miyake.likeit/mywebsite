package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Buy;
import db.DBManager;

public class BuyDAO {

	//購入品登録
	public int insertBuy(int userId, int totalPrice, int deliveryId) {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_buy (user_id, total_price, delivery_method_id, create_date) VALUES (?, ?, ?, now())",
					Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, userId);
			st.setInt(2, totalPrice);
			st.setInt(3, deliveryId);
			st.executeUpdate();
			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting buy-datas has been completed");
			return autoIncKey;
		} catch(SQLException e) {
			e.printStackTrace();
			return -1;
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			} catch(SQLException e) {
				e.printStackTrace();
				return -1;
			}
		}
	}
	//buyIdによる購入品検索
	public Buy getBuyId(int buyId) {

		Connection con = null;
		PreparedStatement st = null;
		try {
			String sql = "SELECT * FROM t_buy"
					+ " JOIN m_delivery_method"
					+ " ON t_buy.delivery_method_id = m_delivery_method.id"
					+ " WHERE t_buy.id = ?";
			con = DBManager.getConnection();
			st = con.prepareStatement(sql);
			st.setInt(1, buyId);
			ResultSet rs = st.executeQuery();
			Buy buy = new Buy();
			while(rs.next()) {
				buy.setBuyId(rs.getInt("id"));
				buy.setTotalPrice(rs.getInt("total_price"));
				buy.setCreateDate(rs.getDate("create_date"));
				buy.setDeliveryId(rs.getInt("delivery_method_id"));
				buy.setUserId(rs.getInt("user_id"));
				buy.setDeliveryPrice(rs.getInt("price"));
				buy.setDeliveryName(rs.getString("name"));
			}
			return buy;
		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			} catch(SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	//userIdによる全検索
	public List<Buy> getBuyList(int userId) {

		Connection con = null;
		PreparedStatement st = null;
		try {
			String sql = "SELECT * FROM t_buy"
					+ " JOIN m_delivery_method"
					+ " ON t_buy.delivery_method_id = m_delivery_method.id"
					+ " WHERE t_buy.user_id = ?"
					+ " ORDER BY t_buy.create_date DESC";
			con = DBManager.getConnection();
			st = con.prepareStatement(sql);
			st.setInt(1, userId);
			ResultSet rs = st.executeQuery();
			List<Buy> buyList = new ArrayList<>();
			while(rs.next()) {
				Buy buy = new Buy();
				buy.setBuyId(rs.getInt("id"));
				buy.setTotalPrice(rs.getInt("total_price"));
				buy.setCreateDate(rs.getDate("create_date"));
				buy.setDeliveryId(rs.getInt("delivery_method_id"));
				buy.setUserId(rs.getInt("user_id"));
				buy.setDeliveryPrice(rs.getInt("price"));
				buy.setDeliveryName(rs.getString("name"));
				buyList.add(buy);
			}
			return buyList;
		} catch(SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			} catch(SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}

}
