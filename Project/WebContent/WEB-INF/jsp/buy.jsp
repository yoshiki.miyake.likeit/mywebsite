<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[購入画面]音楽専門通販サイト＜名前未定＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <div class="example">
        <br>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <div align="right" class="col-sm-11">
        <c:choose>
        <c:when test="${userInfo.loginId == 'admin'}">
            ${userInfo.loginId}さん <a href="Logout">ログアウト</a>
        </c:when>
        <c:otherwise>
        	${userInfo.name}さん<a href="Logout">ログアウト</a>
        </c:otherwise>
        </c:choose>
        </div>
        <div align="center">
            <h1><a href="#" class="text-danger">音楽専門通販サイト＜名前未定＞</a></h1>
        </div>
    </div>
    <br>
    <div align="center">
        <h2>購入画面</h2>
    </div>
    <br>
    <div align="center">
        <a href="InCart" class="btn btn-outline-primary">カートに戻る</a>
    </div>
    <br>
    <div class="container">
    <div class="row center">
    <div class="section">
    <div class="row">
    <div class="card-group">
    <c:forEach var="cart" items="${cart}">
        <div class="card">
            <img src="img/${cart.fileName}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${cart.itemName}</h5>
                <p class="card-text">
                	${cart.categoryName}<br>
                	${cart.usneCategoryName}<br>
                	${cart.PRICE}円
                </p>
            </div>
            <div class="card-footer">
                <a href="ItemDetail?id=${cart.itemId}" class="text-muted">詳　細</a>
            </div>
        </div>
        <c:if test="${(status.index+1) % 4 == 0 }">
        </div>
        <div class="row">
	</c:if>
    </c:forEach>
    </div>
    </div>
    </div>
    </div>
    <br>
    <table class="table table-active">
        <thead>
            <tr>
                <th scope="col">配送方法</th>
                <th scope="col">${DMB.deliveryName}</th>
                <th scope="col">${DMB.deliveryPrice}円</th>
            </tr>
        </thead>
        <tbody>
            <tr class="table table-danger">
                <td></td>
                <td>合計</td>
                <td>${bdb.totalPrice}円</td>
            </tr>
        </tbody>
    </table>
    <br>
    <form action="BuyConfirm" method="post">
    <div align="center">
        <input type="submit" class="btn btn-outline-danger" value="　購　　　入　">
    </div>
    </form>
</body></html>
