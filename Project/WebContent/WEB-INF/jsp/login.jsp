<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>音楽専門通販サイト＜名前未定＞ログイン画面</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
	<script type="text/javascript">
        	$(document).ready(function(){
            	$('.slider').bxSlider({
                	auto: true,
                	pause: 5000,
           	 });
        	});
	</script>


</head>

<body>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
    <div class="example">
        <br>
        <div align="center">
            <h1 class="text-danger">音楽専門通販サイト＜名前未定＞</h1>
        </div>
    </div>
    <br>
    <div align="center">
        <h2>ログイン画面</h2>
    </div>
    <br>
    <br>
    <div class="container">
    <div class="slider">
	<img src="img/kazematiroman.jpg" width="300" height="300" alt="">
	<img src="img/gibsonSGb1.jpeg" width="300" height="300" alt="">
	<img src="img/sakanaction.jpeg" width="300" height="300" alt="">
	<img src="img/speaker2.jpeg" width="300" height="300" alt="">
	</div>
	</div>
    <br>
    <div align="center">
        <p>⬇️通販サイト詳細⬇️</p>
        <a href="#" class="text-danger">サイケデリックな音楽をあなたに</a>
    </div>
    <br>
    <br>
    <form class="form-signin" action="Login" method="post">
    <div align="center">
        ログインID　　　<input type="text" name="loginId">
    </div>
    <br>
    <br>
    <div align="center">
        パスワード　　　<input type="password" name="password">
    </div>
    <br>
    <br>
    <br>
    <div align="center">
        <input type="submit" value=" ロ グ イ ン ">
    </div>
    </form>
    <br>
    <div align="right" class="col-sm-11">
		<a href="Signup">新規登録の方はこちら</a>
    </div>
</body></html>
