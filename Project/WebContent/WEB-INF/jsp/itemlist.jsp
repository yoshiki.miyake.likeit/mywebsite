<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[商品一覧画面]音楽専門通販サイト＜名前未定＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <div class="example">
        <br>
        <div align="right" class="col-sm-11">
            ${userInfo.name}さん <a href="Logout">ログアウト</a>
        </div>
        <div align="center">
            <h1><a href="#" class="text-danger">音楽専門通販サイト＜名前未定＞</a></h1>
        </div>
    </div>
    <a href="#" class="material-icons">music_note</a>
    <a href="UserDetail" class="btn btn-outline-success">ユーザー情報詳細画面へ</a>
    <a href="InCart" class="btn btn-outline-danger">カート画面へ</a>
    <a href="#" class="material-icons">music_note</a>
    <br><br>
    <div align="center">
        <h2>商品一覧画面</h2>
    </div>
    <br>
    <br>
    <div align="center">
    	↓　カテゴリー検索　↓<br>
        <a href="ItemCategoryMasta?id=1" class="btn btn-primary">CD</a>
        <a href="ItemCategoryMasta?id=2" class="btn btn-secondary">レコード</a>
        <a href="ItemCategoryMasta?id=3" class="btn btn-success">DVD</a>
        <a href="ItemCategoryMasta?id=4" class="btn btn-danger">本</a>
        <a href="ItemCategoryMasta?id=5" class="btn btn-warning">楽器</a>
        <a href="ItemCategoryMasta?id=6" class="btn btn-info">スピーカー</a>
    </div>
    <br>
    <br>
    <br>
    <form action="ItemMastaList">
    <div align="center">
        <input type="text" name="search_word" placeholder="例：サカナクション">
        <input class="btn btn-outline-secondary" type="submit" value="検　索">
    </div>
    </form>
    <br>
    <p>↓あなたへのおすすめ↓</p>
    <br>
    <div class="card-deck">
    <c:forEach var="item" items="${itemList}">
         <div class="card">
            <img src="img/${item.fileName}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${item.itemName}</h5>
                <p class="card-text">
                	${item.categoryName}<br>
                	${item.usneCategoryName}<br>
                	${item.PRICE}円
                </p>
            </div>
            <div class="card-footer">
                <a href="ItemDetail?id=${item.itemId}" class="text-muted">詳　細</a>
            </div>
        </div>
    </c:forEach>
    </div>
    <br>
    <p>↓ カテゴリー　：　${cate.categoryName} ↓</p>
    <br>
   <div class="card-deck">
     <c:forEach var="category" items="${categoryList}">
        <div class="card">
            <img src="img/${category.fileName}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${category.itemName}</h5>
                <p class="card-text">
                	${category.usneCategoryName}<br>
                	${category.PRICE}
                </p>
            </div>
            <div class="card-footer">
                <a href="ItemDetail?id=${category.itemId}" class="text-muted">詳　細</a>
            </div>
        </div>
        </c:forEach>
    </div>
    <br>
    <p>↓ カテゴリー ：　${usne.usedNewCategoryName} ↓</p>
    <br>
    <div class="card-deck">
     <c:forEach var="category" items="${usnecategory}">
        <div class="card">
            <img src="img/${category.fileName}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${category.itemName}</h5>
                <p class="card-text">
                	${category.usneCategoryName}<br>
                	${category.PRICE}
                </p>
            </div>
            <div class="card-footer">
                <a href="ItemDetail?id=${category.itemId}" class="text-muted">詳　細</a>
            </div>
        </div>
        </c:forEach>
    </div>
</body></html>
