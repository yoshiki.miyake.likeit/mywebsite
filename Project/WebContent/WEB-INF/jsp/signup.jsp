<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[新規登録画面]音楽専門通販サイト＜名前未定＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body class="sample">
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
    <div class="example">
        <br>
        <div align="center">
            <h1><a class="text-danger">音楽通販サイト＜名前未定＞</a></h1>
        </div>
    </div>
    <br>
    <div align="center">
        <h2>新規登録</h2>
    </div>
    <br>
    <br>
    <div align="center">
        <p>⬇️通販サイト詳細⬇️</p>
        <a href="#" class="text-danger">サイケデリックな音楽をあなたに</a>
        <br>
        <br>
        <p>以下の項目を全て記入してください</p>
    </div>
    <br>
    <form class="form-signin" action="Signup" method="post">
    <div align="center">
        <div class="row">
            <div align="right" class="col-sm-5">
                ログインID<br>
                (必須)
            </div>
            <div class="col-sm-5">
                <input type="text" name="login_id">
            </div>
        </div>
        <br>
        <div class="row">
            <div align="right" class="col-sm-5">
                パスワード<br>
                (必須)
            </div>
            <div class="col-sm-5">
                <input type="password" name="password1">
            </div>
        </div>
        <br>
        <div class="row">
            <div align="right" class="col-sm-5">
                パスワード(確認用)<br>
                (必須)
            </div>
            <div class="col-sm-5">
                <input type="password" name="password2">
            </div>
        </div>
        <br>
        <div class="row">
            <div align="right" class="col-sm-5">
                名前<br>
                (必須)
            </div>
            <div class="col-sm-5">
                <input type="text" name="name">
            </div>
        </div>
        <br>
        <div class="row">
            <div align="right" class="col-sm-5">
                生年月日<br>
                (必須)
            </div>
            <div class="col-sm-5">
                <input type="date" name="birthDate">
            </div>
        </div>
        <br>
        <div class="row">
            <div align="right" class="col-sm-5">
                電話番号
            </div>
            <div class="col-sm-5">
                <input type="tel" name="tel" placeholder="例：012-3456-7890">
            </div>
        </div>
        <br>
        <div class="row">
            <div align="right" class="col-sm-5">
                メールアドレス
            </div>
            <div class="col-sm-5">
                <input type="email" name="mail" placeholder="例：onngaku@docomo.ne.jp">
            </div>
        </div>
        <br>
        <div class="row">
            <div align="right" class="col-sm-5">
                住所　　郵便番号<br><br>
                　　　　都道府県<br><br>
                　　　　市区町村<br><br>
                　　　　番地・マンション名<br><br>
            </div>
            <div class="col-sm-5">
                <p><input type="text" name="address1" placeholder="例：〒123-4567"></p>
                <p><input type="text" name="address2" placeholder="例：東京都"></p>
                <p><input type="text" name="address3" placeholder="例：新宿区"></p>
                <p><input type="text" name="address4" placeholder="例：〇〇番地〇〇号　〇〇アパート〇〇号室"></p>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div align="center">
        <input type="submit" value=" 新 規 登 録 ">
    </div>
     </form>
    <br>
    <div align="right" class="col-sm-11">
        <a href="Login">戻る</a>
    </div>
</body></html>
