<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[購入完了画面]音楽専門通販サイト＜名前未定＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <div class="example">
        <br>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <div align="right" class="col-sm-11">
        <c:choose>
        <c:when test="${userInfo.loginId == 'admin'}">
            ${userInfo.loginId}さん <a href="Logout">ログアウト</a>
        </c:when>
        <c:otherwise>
        	${userInfo.name}さん<a href="Logout">ログアウト</a>
        </c:otherwise>
        </c:choose>
        </div>
        <div align="center">
            <h1><a href="#" class="text-danger">音楽専門通販サイト＜名前未定＞</a></h1>
        </div>
    </div>
    <br>
    <div align="center">
        <h2>購入完了画面</h2>
    </div>
    <br>
    <br>
    <biv align="center">
        <h3>
            <p>ご購入ありがとうございます。</p>
        </h3>
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">購入情報</th>
                    <th scope="col">配送方法</th>
                    <th scope="col">購入品合計金額</th>
                    <th scope="col">購入日時</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row"></th>
                    <td>${buyData.deliveryName}</td>
                    <td>${buyData.TOTALPRICE}</td>
                    <td>${buyData.createDate}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <table class="table table-bordered table-dark">
            <thead>
                <tr>
                    <th>購入品詳細</th>
                    <th scope="col">商品名</th>
                    <th scope="col">金額</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            <c:forEach var="buyItemList" items="${buyItemList}">
                <tr>
                    <td><a href="ItemDetail?id=${buyItemList.itemId}" class="btn btn-light" aria-pressed="true">商品詳細</a></td>
                    <td>${buyItemList.itemName}</td>
                    <td>${buyItemList.PRICE}</td>
                    <td></td>
                </tr>
            </c:forEach>
                <tr>
                    <th scope="row"></th>
                    <td>配送料</td>
                    <td>${buyData.deliveryPrice}</td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </biv>
    <br>
    <br>
    <div align="center">
    <c:choose>
        <c:when test="${userInfo.loginId == 'admin'}">
            <a href="AdminItemList" class="btn btn-outline-primary">商品一覧画面へ（引き続き買い物を続ける）</a>
        	<a href="AdminUserDetail?id=${userInfo.userId}" class="btn btn-outline-danger">　ユ　ー　ザ　ー　詳　細　画　面　へ　</a>
        </c:when>
        <c:otherwise>
        	<a href="ItemList" type="button" class="btn btn-outline-primary">商品一覧画面へ（引き続き買い物を続ける）</a>
        	<a href="UserDetail" type="button" class="btn btn-outline-danger">　ユ　ー　ザ　ー　詳　細　画　面　へ　</a>
        </c:otherwise>
    </c:choose>
    </div>
    <br>
    <br>
    <br>
</body></html>
