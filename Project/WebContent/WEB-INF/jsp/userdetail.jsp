<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[ユーザー情報画面]音楽専門通販サイト＜変態的音楽作家＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <div class="example">
        <br>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <div align="right" class="col-sm-11">
            ${userInfo.name}さん <a href="Logout">ログアウト</a>
        </div>
        <div align="center">
            <h1><a href="#" class="text-danger">音楽専門通販サイト＜変態的音楽作家＞</a></h1>
        </div>
    </div>
    <br>
    <div align="center">
        <h2>ユーザー情報画面</h2>
    </div>
    <br>
    <br>
    <table class="table table-bordered">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">ユーザー情報　詳細　・　更新　・　削除</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row"></th>
                <td>ログインID</td>
                <td>${user.loginId}</td>
            </tr>
            <tr class="table-secondary">
                <th scope="row"></th>
                <td>名　前</td>
                <td>${user.name}</td>
            </tr>
            <tr class="table-primary">
                <th scope="row"></th>
                <td>生年月日</td>
                <td>${user.birthDate}</td>
            </tr>
            <tr class="table-danger">
                <th scope="row"></th>
                <td>電話番号</td>
                <td>${user.tel}</td>
            </tr>
            <tr class="table-warning">
                <th scope="row"></th>
                <td>メールアドレス</td>
                <td>${user.mail}</td>
            </tr>
            <tr class="table-info">
                <th scope="row">住　所</th>
                <td>郵便番号</td>
                <td>${user.postalCode}</td>
            </tr>
            <tr class="table-dark">
                <th scope="row"></th>
                <td>都道府県</td>
                <td>${user.prefectures}</td>
            </tr>
            <tr class="bg-warning">
                <th scope="row"></th>
                <td>市区町村</td>
                <td>${user.city}</td>
            </tr>
            <tr class="table-success">
                <th scope="row"></th>
                <td>番地・マンション名</td>
                <td>${user.addapaName}</td>
            </tr>
            <tr>
                <th scope="row"></th>
                <td><a href="UserUpdate" class="btn btn-outline-danger" role="button" aria-pressed="true">更　新</a></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <br>
    <div align="right" class="col-sm-11">
        <a href="ItemList">商品一覧画面に戻る</a>
    </div>
    <br>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">購入情報</th>
                <th scope="col">購入品合計金額</th>
                <th scope="col">購入品総数</th>
                <th scope="col">購入日時</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="buyList" items="${buyList}">
            <tr>
                <th scope="row"><a href="BuyDetail?id=${buyList.buyId}" class="btn btn-outline-dark" aria-pressed="true">詳　細</a></th>
                <td>${buyList.TOTALPRICE}円</td>
                <td>${buyList.deliveryName}</td>
                <td>${buyList.createDate}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</body></html>
