<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[管理者専用画面]音楽専門通販サイト＜名前未定＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
    <div class="bg_test">
        <br>
        <div align="right" class="col-sm-11">
            ${userInfo.loginId}さん <a href="Logout">ログアウト</a>
        </div>
        <div align="center">
            <h1><a href="#" class="text-danger">管理者専用音楽専門通販サイト＜名前未定＞</a></h1>
        </div>
    </div>
    <br>
    <div align="center">
        <h2>管理者商品登録画面</h2>
        <p>※商品カテゴリー  1:CD 2:レコード 3:DVD 4:本 5:楽器 6:スピーカー
        <p>※商品状態  1:新品 2:中古
    </div>
    <br>
    <form class="form-signin" action="AdminItemInsert" method="post" enctype="multipart/form-data">
    <div class="row">
        <div align="right" class="col-sm-5">
            商品カテゴリー<br>
            (小文字で記入)
        </div>
        <div class="col-sm-5">
            <input type="text" name="itemCategoryId">
        </div>
    </div>
    <br>
    <div class="row">
        <div align="right" class="col-sm-5">
            商品状態<br>
            (小文字で記入)
        </div>
        <div class="col-sm-5">
            <input type="text" name="usneCategoryId">
        </div>
    </div>
    <br>
    <div class="row">
        <div align="right" class="col-sm-5">
            商品名
        </div>
        <div class="col-sm-5">
            <input type="text" name="name">
        </div>
    </div>
    <br>
    <div class="row">
        <div align="right" class="col-sm-5">
            商品詳細
        </div>
        <div class="col-sm-5">
            <input type="text" name="detail">
        </div>
    </div>
    <br>
    <div class="row">
        <div align="right" class="col-sm-5">
            価格<br>
            (小文字で記入)
        </div>
        <div class="col-sm-5">
            <input type="text" name="price">円
        </div>
    </div>
    <br>
    <div class="row">
        <div align="right" class="col-sm-5">
            ファイル名
        </div>
        <div class="col-sm-5">
            <input type="file" name="ck01" accept="image/jpeg" size="50">
        </div>
    </div>
    <br>
    <br>
    <br>
    <div align="center">
        <input type="submit" value=" 商　品　登　録 ">
    </div>
    </form>
    <br>
    <div align="right" class="col-sm-11">
        <a href="AdminItemList">管理者アイテムリスト画面へ</a>
    </div>
</body></html>