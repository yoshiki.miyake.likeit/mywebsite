<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[管理者専用商品一覧画面]音楽専門通販サイト＜名前未定＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
	<div class="bg_test">
    <br>
    <div align="right" class="col-sm-11">
        ${userInfo.loginId}さん <a href="Logout">ログアウト</a>
    </div>
    <div align="center">
        <h1><a href="#" class="text-danger">管理者専用音楽専門通販サイト＜名前未定＞</a></h1>
    </div>
    </div>
    <br>
    <div align="center">
        <h2>管理者用商品一覧画面</h2>
        <c:if test="${category != null}">
        <p>
        	カテゴリー　：　${category.categoryName}
        </p>
        </c:if>
        <p>
			検索結果${itemCount}件
		</p>
    </div>
    <br>
    <c:choose>
		<c:when test="${categoryId == null}">
   			 <form action="AdminItemMastaList">
    			<div align="center">
        			<input type="text" name="search_word" placeholder="例：サカナクション">
        			<input class="btn btn-outline-secondary" type="submit" value="検　索">
   				 </div>
    		</form>
    	</c:when>
		<c:otherwise>
			<form action="ItemCategoryMasta">
    			<div align="center">
        			<input type="text" name="search_word" placeholder="例：サカナクション">
        			<input class="btn btn-outline-secondary" type="submit" value="検　索">
    			</div>
   			 </form>
		</c:otherwise>
	</c:choose>
    <br>
    <br>
    <div align="center">
        <a href="AdminItemInsert" class="btn btn-outline-primary">商品新規登録画面へ</a>
    </div>
    <br>
    <div class="container">
    <div class="row center">
    <div class="section">
    <div class="row">
    <div class="card-deck">
    <c:forEach var="item" items="${itemList}" varStatus="status">
        <div class="card">
            <img src="img/${item.fileName}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${item.itemName}</h5>
                <p class="card-text">
                	${item.categoryName}<br>
                	${item.usneCategoryName}<br>
                	${item.PRICE}円
                </p>
            </div>
            <div class="card-footer">
                <a href="ItemDetail?id=${item.itemId}" class="text-muted">詳　細</a><br>
                <a href="AdminItemUpdate?id=${item.itemId}" class="text-muted">更　新</a><br>
                <a href="AdminItemDelete?id=${item.itemId}" class="text-muted">削　除</a><br>
            </div>
            </div>
    </c:forEach>
    </div>
    </div>
    </div>
    </div>
    </div>
    <br>
    <a href="AdminItemList" class="btn btn-outline-success">管理者アイテムリスト画面へ</a>
    <br>
    <div class="container">
    <div class="row center">
    <c:choose>
	<c:when test="${categoryId == null}">
			<ul class="pagination">
				<!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li class="disabled"><a><i class="material-icons">chevron_left</i></a></li>
					</c:when>
					<c:otherwise>
						<li class="waves-effect"><a href="AdminItemMastaList?search_word=${searchWord}&page_num=${pageNum - 1}"><i class="material-icons">chevron_left</i></a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li <c:if test="${pageNum == status.index }"> class="active" </c:if>><a href="AdminItemMastaList?search_word=${searchWord}&page_num=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<li class="disabled"><a><i class="material-icons">chevron_right</i></a></li>
				</c:when>
				<c:otherwise>
					<li class="waves-effect"><a href="AdminItemMastaList?search_word=${searchWord}&page_num=${pageNum + 1}"><i class="material-icons">chevron_right</i></a></li>
				</c:otherwise>
				</c:choose>
			</ul>
		</c:when>
		<c:otherwise>
		<ul class="pagination">
				<!-- １ページ戻るボタン  -->
				<c:choose>
					<c:when test="${pageNum == 1}">
						<li class="disabled"><a><i class="material-icons">chevron_left</i></a></li>
					</c:when>
					<c:otherwise>
						<li class="waves-effect"><a href="ItemCategoryMasta?search_word=${searchWord}&page_num=${pageNum - 1}"><i class="material-icons">chevron_left</i></a></li>
					</c:otherwise>
				</c:choose>

				<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li <c:if test="${pageNum == status.index }"> class="active" </c:if>><a href="ItemCategoryMasta?search_word=${searchWord}&page_num=${status.index}">${status.index}</a></li>
				</c:forEach>

				<!-- 1ページ送るボタン -->
				<c:choose>
				<c:when test="${pageNum == pageMax || pageMax == 0}">
					<li class="disabled"><a><i class="material-icons">chevron_right</i></a></li>
				</c:when>
				<c:otherwise>
					<li class="waves-effect"><a href="ItemCategoryMasta?search_word=${searchWord}&page_num=${pageNum + 1}"><i class="material-icons">chevron_right</i></a></li>
				</c:otherwise>
				</c:choose>
			</ul>
		</c:otherwise>
		</c:choose>
	</div>
	 </div>
</body></html>