<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[ユーザー削除画面]音楽専門通販サイト＜名前未定＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <div class="example">
        <br>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <div align="right" class="col-sm-11">
            ${userInfo.loginId}さん <a href="Logout">ログアウト</a>
        </div>
        <div align="center">
            <h1><a href="#" class="text-danger">音楽専門通販サイト＜名前未定＞</a></h1>
        </div>
    </div>
    <br>
    <div align="center">
        <h2>ユーザー削除画面</h2>
    </div>
    <br>
    <br>
    <br>
    <form class="form-signin" action="AdminUserDelete" method="post">
    <div align="center">
    	<p>${user.name} さんのアカウントを削除しますか？</p>
        <a href="AdminUserList" class="btn btn-outline-primary">ユーザーリストに戻る</a>
        <input type="submit" class="btn btn-outline-danger"value="　削　　　　　　　除　">
    </div>
    </form>
    <br>
    <br>
    <br>
</body></html>
