<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[商品詳細画面]音楽専門通販サイト＜名前未定＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <div class="example">
        <br>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <div align="right" class="col-sm-11">
        <c:choose>
        	<c:when test="${userInfo.loginId == 'admin'}">
            	${userInfo.loginId}さん <a href="Logout">ログアウト</a>
            </c:when>
            <c:otherwise>
            	${userInfo.name}さん <a href="Logout">ログアウト</a>
            </c:otherwise>
        </c:choose>
        </div>
        <div align="center">
            <h1><a href="#" class="text-danger">音楽専門通販サイト＜名前未定＞</a></h1>
        </div>
    </div>
    <br>
    <div align="center">
        <h2>商品詳細画面</h2>
    </div>
    <br>
    <br>
    <h6>カテゴリー　：　${item.categoryName}</h6>
    <h6>商品状態　：　${item.usneCategoryName}</h6>
        <img src="img/${item.fileName}" class="rounded float-left" width="200" height="200">
        <h4>商品名　：　${item.itemName}</h4>
        <h5>価格（税込）　：　${item.PRICE}円</h5>
        <p>商品詳細　：　${item.itemDetail}</p>
    <br>
    <br>
    <div align="center" class="col-sm-11">
    <c:choose>
    <c:when test="${userInfo.loginId == 'admin'}">
       	<a href="AdminItemList" class="btn btn-outline-primary">管理者商品一覧に戻る</a>
       	<a href="Cart?id=${item.itemId}" class="btn btn-outline-danger">カートに入れる</a>
    </c:when>
    <c:otherwise>
    	<a href="ItemList" class="btn btn-outline-primary">商品一覧に戻る</a>
       	<a href="Cart?id=${item.itemId}" class="btn btn-outline-danger">カートに入れる</a>
    </c:otherwise>
    </c:choose>
    </div>
</body></html>
