<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[カート画面]音楽専門通販サイト＜名前未定＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
    <div class="example">
        <br>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <a href="#" class="material-icons">music_note</a>
        <div align="right" class="col-sm-11">
            <c:choose>
        	<c:when test="${userInfo.loginId == 'admin'}">
            	${userInfo.loginId}さん <a href="Logout">ログアウト</a>
            </c:when>
            <c:otherwise>
            	${userInfo.name}さん <a href="Logout">ログアウト</a>
            </c:otherwise>
        </c:choose>
        </div>
        <div align="center">
            <h1><a href="#" class="text-danger">音楽専門通販サイト＜名前未定＞</a></h1>
        </div>
    </div>
    <br>
    <div align="center">
        <h2>カート画面</h2>
        <p>${cartActionMessage}
    </div>
    <br>
    <br>
    <div align="center">
    <c:choose>
    <c:when test="${userInfo.loginId == 'admin'}">
        <a href="AdminItemList" class="btn btn-outline-primary">商品一覧へ戻る</a>
        <a href="BuyConfirm" class="btn btn-outline-danger">購入手続きへ</a>
    </c:when>
    <c:otherwise>
    	<a href="ItemList" class="btn btn-outline-primary">商品一覧へ戻る</a>
        <a href="BuyConfirm" class="btn btn-outline-danger">購入手続きへ</a>
    </c:otherwise>
    </c:choose>
    </div>
    <br>
    <div class="container">
    <div class="row center">
    <div class="section">
    <div class="row">
    <div class="card-group">
    <c:forEach var="cart" items="${cart}" varStatus="status">
        <div class="card">
            <img src="img/${cart.fileName}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${cart.itemName}</h5>
                <p class="card-text">
                ${cart.categoryName}<br>
                ${cart.usneCategoryName}<br>
                ${cart.PRICE}円
                </p>
            </div>
            <div class="card-footer">
            	<a href="ItemDetail?id=${cart.itemId}" class="text-muted">詳　細</a><br>
                <a href="CartItemDelete?id=${cart.itemId}" class="text-muted">削　除</a>
            </div>
        </div>
        <c:if test="${(status.index+1) % 4 == 0 }">
        </div>
        <div class="row">
	</c:if>
    </c:forEach>
    </div>
    </div>
    </div>
    </div>

</body></html>
