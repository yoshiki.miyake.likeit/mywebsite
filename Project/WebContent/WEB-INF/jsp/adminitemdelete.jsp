<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>[管理者専用画面]音楽専門通販サイト＜名前未定＞</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div class="bg_test">
        <br>
        <div align="right" class="col-sm-11">
            ${userInfo.loginId}さん <a href="Logout">ログアウト</a>
        </div>
        <div align="center">
            <h1><a href="#" class="text-danger">管理者専用音楽専門通販サイト＜名前未定＞</a></h1>
        </div>
    </div>
    <br>
    <div align="center">
        <h2>管理者商品削除画面</h2>
    </div>
    <br>
    <div align="center">
        <img src="img/${item.fileName}" class="img-fluid" align="left">
        商品名：${item.itemName}
        <br>
        商品詳細：${item.itemDetail}
        <br>
        価格：${item.price}円
    </div>
    <br>
    <br>
    <div align="center">
        <h5>本当にこの商品をリストから削除しますか？</h5>
        <br>
        <form action="AdminItemDelete" method="post">
        	<a href="AdminItemList" class="btn btn-outline-primary">いいえ（管理者アイテムリスト画面へ）</a>
        	<input type="submit" class="btn btn-outline-danger" value="　　は　　　　　　　　　　い　　">
        </form>
    </div>
</body></html>