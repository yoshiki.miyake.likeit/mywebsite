create database MyWebSite DEFAULT CHARACTER SET utf8;
use MyWebSite;
CREATE TABLE t_user (
  id int PRIMARY KEY AUTO_INCREMENT,
  login_id varchar(256) unique not null,
  password varchar(256) not null,
  name varchar(256) not null,
  birth_data date default null,
  tel varchar(256) default null,
  mail varchar(256) default null,
  postal_code varchar(256) default null,
  prefectures varchar(256) default null,
  city varchar(256) default null,
  address_apartment_name varchar(256) default null,
  create_date date not null
);
CREATE TABLE t_buy_detail (
  id int PRIMARY KEY AUTO_INCREMENT,
  buy_id int NOT NULL,
  item_id int NOT NULL
);
CREATE TABLE t_buy (
  id int PRIMARY KEY AUTO_INCREMENT,
  user_id int NOT NULL,
  total_price int NOT NULL,
  delivery_method_id int NOT NULL,
  create_date datetime NOT NULL
);
CREATE TABLE m_delivery_method (
  id int PRIMARY KEY AUTO_INCREMENT,
  name varchar(256) NOT NULL,
  price int NOT NULL
);
CREATE TABLE m_item (
  id int primary key AUTO_INCREMENT,
  item_category_id int not null,
  used_new_category_id int not NULL,
  name varchar(256) not NULL,
  detail text ,
  price int not NULL,
  file_name varchar(256) not NULL
);
create table category (
 id int primary key AUTO_INCREMENT,
 category_name varchar(256) not null
);
create table used_new_category (
 id int primary key AUTO_INCREMENT,
 used_new_category_name varchar(256) not null
);

insert into `m_delivery_method` value (1, '通常配送', 500);

insert into `used_new_category` value (1, '新品');
insert into `used_new_category` value (2, '中古');

insert into `category` value (1, 'CD');
insert into `category` value (2, 'レコード');
insert into `category` value (3, 'DVD');
insert into `category` value (4, '本');
insert into `category` value (5, '楽器');
insert into `category` value (6, 'スピーカー');


